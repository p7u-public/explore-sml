val c1 = ["Several","problems","require","You","to","Use","Certain","library","Functions"];
val c2 = ["k"];
val c3 = ["aymy","wbt","cyut7"];
val c4 = ["aymy696","wbt1234","cyut7"];
val c5 = [];

val check10 = only_capitals ["A","B","C"] = ["A","B","C"]
val check11 = only_capitals c1 = ["Several","You","Use","Certain","Functions"];
val check12 = only_capitals c2 = [];

val check20 = longest_string1 ["A","bc","C"] = "bc"
val check21 = longest_string1 c3 = "cyut7";
val check22 = longest_string1 c4 = "aymy696";
val check23 = longest_string1 c5 = "";

val check30 = longest_string2 ["A","bc","C"] = "bc"
val check31 = longest_string2 c3 = "cyut7";
val check32 = longest_string2 c4 = "wbt1234";
val check33 = longest_string2 c5 = "";

val check4a = longest_string3 ["A","bc","C"] = "bc"

val check4b = longest_string4 ["A","B","C"] = "C"

val check50 = longest_capitalized ["A","bc","C"] = "A";
val check51 = longest_capitalized c1 = "Functions";
val check52 = longest_capitalized c3 = "";

val check60 = rev_string "abc" = "cba";
val check61 = rev_string "behemoth" = "htomeheb";
val check62 = rev_string "a" = "a";

val check70 = first_answer (fn x => if x > 3 then SOME x else NONE) [1,2,3,4,5] = 4

val check80 = all_answers (fn x => if x = 1 then SOME [x] else NONE) [2,3,4,5,6,7] = NONE

val check9a0 = count_wildcards Wildcard = 1
val check9a1 = count_wildcards (TupleP[ConstP(3),ConstructorP("s",Wildcard),UnitP,TupleP[Wildcard,Variable("s")]]) = 2
val check9a2 = count_wildcards (TupleP[ConstP(3),ConstructorP("s",Wildcard),UnitP,TupleP[Wildcard,Variable("s"),ConstructorP("s",Wildcard)]]) = 3
val check9a3 = count_wildcards (ConstP(8)) = 0

val check9b0 = count_wild_and_variable_lengths (Variable("a")) = 1
val check9b1 = count_wild_and_variable_lengths (TupleP[ConstP(3),ConstructorP("s",Wildcard),UnitP,TupleP[Wildcard,Variable("s")]]) = 3
val check9b2 = count_wild_and_variable_lengths (TupleP[ConstructorP("s",Wildcard),TupleP[Wildcard,Variable("abc")]]) = 5
val check9b3 = count_wild_and_variable_lengths (TupleP[Wildcard,TupleP[Wildcard,Variable("123")],Variable("123")]) = 8
val check9b4 = count_wild_and_variable_lengths Wildcard = 1;

val check9c0 = count_some_var ("x", Variable("x")) = 1
val check9c1 = count_some_var ("123", TupleP[Wildcard,TupleP[Wildcard,Variable("123")],Variable("123")]) = 2
val check9c2 = count_some_var ("123", TupleP[Wildcard,TupleP[Wildcard,Variable("-")],Variable("123")]) = 1