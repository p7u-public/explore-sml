(* Define several SML functions. Many will be very short because they will use other 
higher-order functions. You may use functions in ML's library; the problems point 
you toward the useful functions and often require that you use them. The sample 
solution is about 120 lines, including the provided code, but not including the 
challenge problem. This assignment is probably more difficult than Homework 2 even
though (perhaps because) many of the problems have 1-line answers. *)

(* Provided Code *)

exception NoAnswer

datatype pattern =  Wildcard
                  | Variable of string
                  | UnitP
                  | ConstP of int
                  | TupleP of pattern list
                  | ConstructorP of string * pattern

datatype valu =  Const of int
               | Unit
               | Tuple of valu list
               | Constructor of string * valu

fun g f1 f2 p = let val r = g f1 f2 
                in case p of Wildcard          => f1 ()
                           | Variable x        => f2 x
                           | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
                           | ConstructorP(_,p) => r p
                           | _                 => 0
                end

(* 1 *)
val only_capitals = List.filter(fn str => Char.isUpper(String.sub(str, 0)))

(* 2 *)
val longest_string1 = List.foldl(fn (current, longest) => if String.size(current) 
								> String.size(longest) then current else longest) ""

(* 3 *)								
val longest_string2 = List.foldl(fn (current, longest) => if String.size(current) 
								>= String.size(longest) then current else longest) ""

(* 4a *)
fun longest_string_helper f = List.foldl(fn (current, longest) => if f(String.size current, 
							 	String.size longest) then current else longest) ""

(* 4b *)
val longest_string3 = longest_string_helper op > 

(* 4c *)
val longest_string4 = longest_string_helper op >=

(* 5 *)
val longest_capitalized = longest_string1 o only_capitals

(* 6 *)
val rev_string = String.implode o List.rev o String.explode 

(* 7 *)
fun first_answer f list = 
    case list of
         [] => raise NoAnswer
       | hd :: tl => case f hd of
                          SOME elem => elem
                        | NONE => first_answer f tl

(* 8 *)
fun all_answers f list =
     let fun loop(acc,list) = 
              case list of
                   [] => SOME acc
                 | hd::tl => case f hd of 
                                  NONE => NONE
                                | SOME y => loop((y @ acc), tl)
     in loop([],list) 
     end

(* 9a *)
val count_wildcards = g (fn() => 1) (fn _ => 0)

(* 9b *)
val count_wild_and_variable_lengths = g (fn() => 1) String.size

(* 9c *)
fun count_some_var(string, pattern) = g (fn _ => 0) (fn s => if s = string then 1 else 0) pattern