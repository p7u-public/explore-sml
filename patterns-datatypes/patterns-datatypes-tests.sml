
val c1 = (Clubs,Jack);
val c2 = (Hearts,Num 2);
val c3 = (Spades,Ace);  (* Motörhead *)
val c4 = (Diamonds,Jack);

val check10 = all_except_option("string", ["string"]) = SOME []
val check11 = all_except_option("A",["D","E","F","A","U"]) = SOME(["D","E","F","U"])
val check12 = all_except_option("V",["D","E","F","A","U"]) = NONE
val check13 = all_except_option("U",["D","E","F","A","U"]) = SOME(["D","E","F","A"])

val check20 = get_substitutions1([["foo"],["there"]], "foo") = []
val check21 = get_substitutions1([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],"Fred") 
				= ["Fredrick","Freddie","F"]
val check22 = get_substitutions1([["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],"Jeff")
				= ["Jeffrey","Geoff","Jeffrey"]

val check30 = get_substitutions2([["foo"],["there"]], "foo") = []
val check31 = get_substitutions2([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],"Fred") 
				= ["Fredrick","Freddie","F"]
val check32 = get_substitutions2([["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],"Jeff")
				= ["Jeffrey","Geoff","Jeffrey"]								
				
val check40 = similar_names([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],
							{first="Fred", middle="W", last="Smith"})
				= [{first="Fred", last="Smith", middle="W"}, {first="Fredrick", last="Smith", middle="W"},
				   {first="Freddie", last="Smith", middle="W"}, {first="F", last="Smith", middle="W"}]

	     	     
val check50 = card_color((Clubs, Num 2)) = Black
val check51 = card_color c1 = Black;
val check52 = card_color c2 = Red;
val check53 = card_color c3 = Black;
val check54 = card_color c4 = Red;

val check60 = card_value((Clubs, Num 2)) = 2
val check61 = card_value c1 = 10;
val check62 = card_value c2 = 2;
val check63 = card_value c3 = 11;
val check64 = card_value c4 = 10;

val check70 = remove_card([(Hearts, Ace)], (Hearts, Ace), IllegalMove) = []
val check71 = remove_card([c1,c2,c3,c4], c1, IllegalMove) = [c2,c3,c4]
val check72 = remove_card([c1,c2,c3,c4], c2, IllegalMove) = [c1,c3,c4]
val check73 = remove_card([c1,c2,c3,c4], c3, IllegalMove) = [c1,c2,c4]
val check74 = remove_card([c1,c2,c3,c4], c4, IllegalMove) = [c1,c2,c3]
(*val check75 = remove_card([c1], (Hearts, Ace), IllegalMove) = [c1] -- exception *)

val check80 = all_same_color([(Hearts, Ace), (Hearts, Ace)]) = true
val check81 = all_same_color([c1,c3,(Clubs,Queen)]) = true
val check82 = all_same_color([c1,c3,c2]) = false
val check83 = all_same_color([]) = true
val check84 = all_same_color([c1]) = true

val check90 = sum_cards([(Clubs, Num 2),(Clubs, Num 2)]) = 4
val check91 = sum_cards([c1,c2,c3,c4]) = 33
val check92 = sum_cards([c1]) = 10
val check93 = sum_cards([]) = 0

val test100 = score([(Hearts, Num 2),(Clubs, Num 4)],10) = 4

val test110 = officiate([(Hearts, Num 2),(Clubs, Num 4)],[Draw], 15) = 6     
             
