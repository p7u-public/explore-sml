(* Write SML functions (not counting local helper functions), 4 related to "name substitutions"
and 7 related to a made-up solitaire card game.
Your solutions must use pattern-matching. You may not use the functions null, hd, tl, isSome, 
or valOf, nor may you use anything containing a # character or features not used in class 
(such as mutation). Note that list order does not matter unless specifically stated in the problem. *)

(* If you use this function to compare two strings (returns true if the same string), 
then you avoid several of the functions in problem 1 having polymorphic types that 
may be confusing. *)
fun same_string(s1 : string, s2 : string) = s1 = s2

(* 1a *)
(* Write a function all_except_option, which takes a string and a string list. Return 
NONE if the string is not in the list, else return SOME lst where lst is identical 
to the argument list except the string is not in it. You may assume the string is 
in the list at most once. Use same_string, provided to you, to compare strings. 
Sample solution is around 8 lines. *)

fun all_except_option(sample, strings) =
     case strings of
          [] => NONE
        | head :: tail => if same_string(head, sample)
                          then SOME(tail)
                          else case all_except_option(sample, tail) of
                                         NONE => NONE
                                       | SOME list => SOME(head :: list)

(* 1b *)
(* Write a function get_substitutions1, which takes a string list list (a list of list 
of strings, the substitutions) and a string s and returns a string list. The result 
has all the strings that are in some list in substitutions that also has s, but s 
itself should not be in the result. Assume each list in substitutions has no repeats. 
The result will have repeats if s and another string are both in more than one list 
in substitutions. Use part (a) and ML's list-append (@) but no other helper functions. 
Sample solution is around 6 lines. *)

fun get_substitutions1(strings, sample) = 
     case strings of
          [] => []
        | head :: tail => case all_except_option(sample, head) of
                               SOME list => list @ get_substitutions1(tail, sample)
                             | NONE => get_substitutions1(tail, sample)				
	
(* 1c *)
(* Write a function get_substitutions2, which is like get_substitutions1 except it uses 
a tail-recursive local helper function. *)

fun get_substitutions2(strings, sample) = 
    let fun build(from, result) = 
             case from of
                  [] => result
                | hd :: tl => case all_except_option(sample, hd) of
                                   SOME lst => build(tl, result @ lst)
                                 | NONE => build(tl, result)
    in build(strings, [])
    end

(* 1d *)
(* Write a function similar_names, which takes a string list list of substitutions (as 
in parts (b) and (c)) and a full name of type {first:string,middle:string,last:string}
and returns a list of full names (type {first:string,middle:string,last:string} list). 
The result is all the full names you can produce by substituting for the first name 
(and only the first name ) using substitutions and parts (b) or (c). The answer should 
begin with the original name (then have 0 or more other names).
Do not eliminate duplicates from the answer. Hint: Use a local helper function. Sample 
solution is around 10 lines. *)

fun similar_names(substitutables, name) = 
    let val {first=f, middle=m, last=l} = name
        fun build(list) = 
             case list of
                  [] => []
                | head :: tail => {first=head, middle=m, last=l} :: build(tail)
    in name :: build(get_substitutions1(substitutables, f))
    end
	
(* This problem (2) involves a solitaire card game invented just for this question. 
You will write a program that tracks the progress of a game; writing a game player 
is a challenge problem. You can do parts (a){(e) before understanding the game if 
you wish. A game is played with a card-list and a goal. The player has a list of 
held-cards, initially empty. The player makes a move by either drawing, which means 
removing the first card in the card-list from the card-list and adding it to the 
held-cards, or discarding, which means choosing one of the held-cards to remove. 
The game ends either when the player chooses to make no more moves or when the sum 
of the values of the held-cards is greater than the goal. *)
	
(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)

datatype suit = Clubs | Diamonds | Hearts | Spades

datatype rank = Jack | Queen | King | Ace | Num of int 

type card = suit * rank

datatype color = Red | Black

datatype move = Discard of card | Draw 

exception IllegalMove

(* 2a *)
(* Write a function card_color, which takes a card and returns its color (spades and 
clubs are black, diamonds and hearts are red). Note: One case-expression is enough. *)

fun card_color card =
     case card of
          (Clubs, _) => Black
        | (Spades, _) => Black
        | (Diamonds, _) => Red
        | (Hearts, _) => Red

(* 2b *)
(* Write a function card_value, which takes a card and returns its value (numbered 
cards have their number as the value, aces are 11, everything else is 10). Note: 
One case-expression is enough. *)

fun card_value card =
    case card of
         (_,Jack)  => 10
       | (_,Queen) => 10
       | (_,King) => 10
       | (_,Ace) => 11
       | (_,Num v) => v
    
(* 2c *)      
(* Write a function remove_card, which takes a list of cards cs, a card c, and an 
exception e. It returns a list that has all the elements of cs except c. If c is 
in the list more than once, remove only the rst one. If c is not in the list, 
raise the exception e. You can compare cards with '=' *)

fun remove_card(cards, card, exc) = 
    case cards of
         [] => []
       | head :: tail => if head = card
                         then tail
                         else head :: remove_card(tail, card, exc)
						  	
	  
(* 2d *)
(* Write a function all_same_color, which takes a list of cards and returns true if 
all the cards in the list are the same color. Hint: An elegant solution is very 
similar to one of the functions using nested pattern-matching in the lectures. *)

fun all_same_color cards = 
    case cards of
         hd :: nk :: tl => card_color hd = card_color nk andalso all_same_color(nk :: tl)
       | _ => true
	
(* 2e *)
(* Write a function sum_cards, which takes a list of cards and returns the sum of 
their values. Use a locally defined helper function that is tail recursive. *)

fun sum_cards cards = 
    let fun add(list, sum) = 
            case list of
                 [] => sum
               | hd :: tl => add(tl, sum + card_value hd) 
    in add(cards, 0)
    end
	
(* 2f *)
(* Write a function score, which takes a card list (the held-cards) and an int (the goal) 
and computes the score as described above. *)

fun score(cards, goal) =
    let val sum = sum_cards cards
    in (if sum >= goal then 3 * (sum - goal) else goal - sum) 
       div (if all_same_color cards then 2 else 1)
    end
