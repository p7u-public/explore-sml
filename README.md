# Standard ML (SML) is a general-purpose, modular, functional programming language with compile-time type checking and type inference.

This exploration is based on exercises from Coursera's 'Programming languages' course.
