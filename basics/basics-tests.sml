
    val d1 = (1983,5,28);
    val d2 = (1983,9,12);
    val d3 = (1983,9,10);
    val d4 = (1980,10,1);
    val ds = [d1,d2,d3,d4];
    val dates = [(1,2,98),(3,5,201),(1,17,83),(3,2,2412),(1,2,342),(1,2,98),(6,7,8)] 
    val ms1 = [5,9];
    val ms2 = [9,11,6];
    val ms3 = [1,2,3,4,6,7,8];
    val ms4 = [10,5];
    val ss1 = ["one","two","three","four","five"];
    val numbers = [1, 6, 9, 4, 2, 19];

    val check10 = is_older((1,2,3),(2,3,4)) = true
    val check11 = is_older(d1,d2) = true;
    val check12 = is_older(d2,d1) = false;
    val check13 = is_older(d1,d1) = false;
    val check14 = is_older(d3,d2) = true;
    val check15 = is_older(d2,d3) = false;
    val check16 = is_older(d4,d1) = true;

    val check20 = number_in_month([(2012,2,28),(2013,12,1)],2) = 1
    val check21 = number_in_month(ds,5) = 1;
    val check22 = number_in_month(ds,9) = 2;
    val check23 = number_in_month(ds,7) = 0;
 
    val check30 = number_in_months([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = 3
    val check31 = number_in_months(ds,ms1) = 3;
    val check32 = number_in_months(ds,ms2) = 2;
    val check33 = number_in_months(ds,ms3) = 0;
  
    val check40 = dates_in_month([(2012,2,28),(2013,12,1)],2) = [(2012,2,28)]
    val check41 = dates_in_month(ds,5) = [d1];
    val check42 = dates_in_month(ds,9) = [d2,d3];
    val check43 = dates_in_month(ds,7) = [];
    
    val check50 = dates_in_months([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4])=[(2012,2,28),(2011,3,31),(2011,4,28)]
    val check51 = dates_in_months(ds,ms1) = [d1,d2,d3];
    val check52 = dates_in_months(ds,ms2) = [d2,d3];
    val check53 = dates_in_months(ds,ms3) = [];
    val check54 = dates_in_months(ds,ms4) = [d4,d1];
    val check55 = dates_in_months(dates,[2,5])=[(1,2,98),(3,2,2412),(1,2,342),(1,2,98),(3,5,201)];

    val check60 = get_nth(["hi", "there", "how", "are", "you"], 2)="there"
    val check61 = get_nth(ss1,2) = "two";
    val check62 = get_nth(ss1,1) = "one";

    val check70 = date_to_string((2013, 6, 1))="June 1, 2013"
    val check71 = date_to_string(d1) = "May 28, 1983";
    val check72 = date_to_string(d2) = "September 12, 1983";
    val check73 = date_to_string(d3) = "September 10, 1983";
    val check74 = date_to_string(d4) = "October 1, 1980";

    val check80 = number_before_reaching_sum(10, [1,2,3,4,5]) = 3
    val check81 = number_before_reaching_sum(8, numbers) = 2
    val check82 = number_before_reaching_sum(17, numbers) = 3
    val check83 = number_before_reaching_sum(21, numbers) = 4
    val check84 = number_before_reaching_sum(1, numbers) = 0
    val check85 = number_before_reaching_sum(100, numbers) = 6
    val check86 = number_before_reaching_sum(5, [3,1,2]) = 2
    val check87 = number_before_reaching_sum(4, [1,4,1,1]) = 1
    val check88 = number_before_reaching_sum(6, [4,1,1,1]) = 2

    val check90 = what_month(70) = 3
    val check91 = what_month(31) = 1
    val check92 = what_month(60) = 3
    val check93 = what_month(365) = 12

    val check100 = month_range(31,34) = [1,2,2,2]
    val check101 = month_range(1,31) = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    val check102 = month_range(31,32) = [1,2]
    val check103 = month_range(335,365) = [12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12]
    val check104 = month_range(5,3) = []
    val check105 = month_range(3,3) = [1] 
    val check106 = month_range(85,145) = [3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]

    val check110 = oldest([(2012,2,28),(2011,3,31),(2011,4,28)]) = SOME(2011,3,31)
    val check111 = oldest([(~4,2,3),(2,2,3),(5,2,3),(~3,2,3)]) = SOME(~4,2,3)
    val check112 = oldest([(5,5,2),(5,10,2),(5,2,2),(5,12,2)]) = SOME(5,2,2)
    val check113 = oldest([(5,12,15),(5,12,10),(5,12,1)]) = SOME(5,12,1)
    val check114 = oldest([]) = NONE
